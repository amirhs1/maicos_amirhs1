Kinetic Energy data
===================

A bulk water liquid sample made of 5000 water molecules and exposed to an
alternating electric field.

.. list-table::
   :widths: 25 75
   :header-rows: 1

   * - Property
     - Value

   * - Code
     - GROMACS
   * - Ensemble
     - NVT
   * - Force-field
     - water model = TIP3P
   * - Timestep
     - 2 fs
   * - Frame number
     - 1001
   * - Duration
     - 100 ps
   * - Dimensions
     - 55x55x55 nm**3
   * - Others
     - Temperature 300 K, h-bonds contrained by LINCS, coulombtype = PME,
       Electric field information are : E0 = 0.2, omega = 0.5, t0 = 5.0,
       sigma = 0.5, phi = 0.628319
