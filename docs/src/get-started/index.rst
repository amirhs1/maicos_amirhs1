.. _userdoc-get-started:

Getting started
===============

This sections describes MAICoS, how to install it, and its most basic commands.

.. toctree::
    :maxdepth: 0

    maicos
    installation
    ../examples/usage-python
    usage-bash
    units
    changelog
